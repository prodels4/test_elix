


const appReducer = (state = {
 
    id: ""

}, action) => {

    let newState = JSON.parse(JSON.stringify(state));

    switch (action.type) {

        case 'ASSET_ID':

        newState.id = action.id;

        
        return newState;

        default:
            return state;
    }

}
export default appReducer;