import React from "react";
import Navegacion from "./Navegacion.jsx"
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import appReducer from '../reducers/appReducer'

// Crear almacén Redux
const store = createStore(appReducer);

export default () => (
  <>
    <Provider store={store}>
      <Navegacion />
    </Provider>
  </>
);
