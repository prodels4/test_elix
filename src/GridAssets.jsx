import React, { Component } from 'react'
import { Table } from 'reactstrap';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';

// URL de la API y el PROXY para evitar error de CORS
const proxyUrl = 'https://cors-anywhere.herokuapp.com/'
const API = "https://6y458uslg3.execute-api.eu-west-3.amazonaws.com/elixos/assets";

class GridAssets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
        this.saveId = this.saveId.bind(this);
    }

    // Guardar ID del "Asset" seleccionado en almacén Redux
    saveId(id) {

        this.props.dispatch({

            type: 'ASSET_ID',
            id: id
        })
    }

    // Fetch a la API para llenar el state con los datos necesarios
    componentDidMount() {
        const url = proxyUrl + API;
        fetch(url)
            .then(data => data.json())
            .then(datajs => {

                this.setState({ data: datajs })

            })
            .catch(err => console.log(err));
    }
    render() {


        // Mostrar mensaje de carga mientas esperamos al fetch
        if (this.state.data.length === 0) {
            return <h3>Cargando datos...</h3>;
        }

        // Llenar la tabla con los datos 
        let datos = this.state.data.assets.map(el =>
            <tr key={el.id}>
                <td>{el.id}</td>
                <td>{el.t_street_name}</td>
                <td>{el.n_number}</td>
                <td>{el.t_city}</td>
                <td>{el.t_code}</td>
                <td><Link onClick={() => this.saveId(el.id)} to={"/entities"} className="btn btn-success">Selecciona</Link></td>
            </tr>
        )

        return (
            <div>

                <Table>
                    <thead>

                    </thead>
                    <tbody>
                        {datos}
                    </tbody>
                </Table>

            </div>
        )
    }
}

export default connect()(GridAssets);