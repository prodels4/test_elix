import React, { Component } from 'react'
import { Table } from 'reactstrap';
import { connect } from 'react-redux';


// URL de la API y el PROXY para evitar error de CORS
const API = "https://6y458uslg3.execute-api.eu-west-3.amazonaws.com/elixos/entities";
const proxyUrl = 'https://cors-anywhere.herokuapp.com/';

class GridEntities extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    // Fetch a la API para llenar el state con los datos necesarios
    componentDidMount() {
        const url = proxyUrl + API;
        fetch(url)
            .then(data => data.json())
            .then(datajs => {

                this.setState({ data: datajs })

            })
            .catch(err => console.log(err));
    }

    render() {

        
        // Mostrar mensaje de carga mientas esperamos al fetch
        if (this.state.data.length === 0) {
            return <h3>Cargando datos...</h3>;
        }

        let arrayEntidades = JSON.parse(JSON.stringify(this.state.data.entities))

        // Filtrar datos por el id del "Asset" guardado en Redux
        let datosFiltrados = arrayEntidades.filter(el => el.id_asset==this.props.id)


        // Llenar la tabla con los datos 
        let datos = datosFiltrados.map(el =>
            <tr key={el.id}>
                <td>{el.id_asset}</td>
                <td>{el.id_entry}</td>
                <td>{el.t_entry_use}</td>
                <td>{el.t_entry_building}</td>
            </tr>
        )
        return (
            <div>
                <Table>
                    <thead>

                    </thead>
                    <tbody>
                        {datos}
                    </tbody>
                </Table>
            </div>
        )
    }
}

// Importar id del "Asset" desde Redux
const mapStateToProps = (state) => {
    return {
        id: state.id
    }
}
export default connect(mapStateToProps)(GridEntities);
