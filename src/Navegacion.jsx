import React, { Component } from 'react';
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Navbar } from 'reactstrap';
import Assets from "./Assets.jsx";
import Entities from "./Entities.jsx";



export default class Navegacion extends Component {


    render() {


        return (

            // Navbar principal de la aplicacion con enrutamiento.
            <>
                <BrowserRouter>
                    <div>
                        <Navbar color="light" light expand="md">
                            <Link to="/" className="nav-link active"> Assets</Link>
                            {/* <Link to="/entities" className="nav-link active"> Entities</Link> */}
                        </Navbar>
                    </div>

                    {/* Rutas principales  */}
                    <Switch>
                        <Route exact path="/" component={Assets} />
                        <Route path="/entities" component={Entities} />
                    </Switch>
                </BrowserRouter>
            </>
        )
    }
}

